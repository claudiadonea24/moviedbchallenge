# MovieDB challenge

A **ReactJS** aplication build with **webpack** and tested with **Jasmine**

### Instalation 
npm i

### Start Dev Server
npm run dev

#### Test
```npm run test```

#### Linter
```npm run lint```


### Features

* ES linter ✔ 
* Map ✔
* Buildable

* Tests -> Render  ✔ 
		-> Requests

## TO DO

* Memoisation
* Coverage 

For better dev experience use [React Dev Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi/related)
 