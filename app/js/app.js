import React from 'react';
import { render } from 'react-dom';
import $ from 'jquery';

import Main from '../components/main/js/main';

class App extends React.Component {
    render() {
        return  <Main></Main>;
    }
}

$(document).ready(function () {
  render(<App/>, document.querySelector('#app'));
});
