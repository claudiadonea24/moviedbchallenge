import $ from 'jquery';
import	moviedb from '../../../libs/themoviedb';

module.exports = () => {
	
	let config =  {
		urlBase: 'http://api.themoviedb.org/3/search/multi/',
		API_KEY:'893a084a5ec1d237ec89cc98361d1dc6',
	};
	/*
	* @function succes
	* @param {array} response
	* processes the response and updates the list state
	* [TO DO] to replace list so it can update any state
	*/
	let success = function (response) {
		response = JSON.parse(response);
		this.setState({
			list: response.results
		});
	};
	/*
	* @function error
	* @param {object} err
	*/
	let error = function(err){
		console.error(err);
	}
	/*
	* @function searchMovie
	* @param {String} query
	* binds this to succes function and requests for a list of movies
	* [TO DO] to replace list so it can update any state
	*/
	let searchMovie = function(query) {
		success = success.bind(this);
		if(query && query.length < 3){
	    	moviedb.search.getMovie({'query': query}, success, error);
		}
	}
	/*
	* @function getNowPlaying
	* @param {String} query
	* binds this to succes function and requests for a list of movies that are currently in cinemas
	*/
	let getNowPlaying = function(query) {
		success = success.bind(this);
		moviedb.movies.getNowPlaying({'query': ''},success,error);
	}

	return {
		searchMovie: searchMovie,
		getNowPlaying: getNowPlaying
	}
}();