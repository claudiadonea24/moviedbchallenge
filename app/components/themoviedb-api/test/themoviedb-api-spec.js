import themoviedbApi from '../js/themoviedb-api';
import	moviedb from '../../../libs/themoviedb';

describe('Test moviedb-api', () => {
    beforeEach(function() {
      //jasmineAjax.Ajax.install();
    });
   it('searchMovie() shouldn\'t call getMovie if query < 3', () => { 
		let query = '';     
        spyOn(moviedb.search,'getMovie');
        themoviedbApi.searchMovie(query);
        expect(moviedb.search.getMovie).not.toHaveBeenCalled();
   });

   it('searchMovie() should call getMovie if query >= 2', () => { 
		let query = 'test';     
        spyOn(moviedb.search,'getMovie');
        themoviedbApi.searchMovie(query);
        expect(moviedb.search.getMovie).toHaveBeenCalled();
   });

   it('searchMovie() should call succes getNowPlaying', () => {
       let query = '';     
        spyOn(moviedb.movies,'getNowPlaying');
        themoviedbApi.getNowPlaying(query);
        expect(moviedb.movies.getNowPlaying).toHaveBeenCalled();
   });
});
