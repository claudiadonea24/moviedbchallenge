import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';

import Main from '../js/main';


describe('Testing main component', () => {
   
   it('should display the main component', () => {
       const mainComponent = TestUtils.renderIntoDocument(
       		<Main></Main>
       	);
       
       expect(ReactDOM.findDOMNode(mainComponent).tagName).toEqual('DIV');
   });
});
