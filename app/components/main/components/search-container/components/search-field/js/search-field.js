import React from 'react';
import { render } from 'react-dom';

let SearchField = React.createClass ({
	/*
	* @method handleChange
	* passes to parent a handler the handler for change input
	*/
	handleChange() {
		this.props.onUserInput(this.refs.filterTextInput.value);
	},
	render() {
        return <form className="search-form">
        	<input 
        		type="text" 
        		placeholder="Search for a movie..." 
        		value={this.props.searchQuery}
        		ref="filterTextInput"
        		onChange={this.handleChange}
    		/>
        </form>;
    }
})

export default SearchField;
