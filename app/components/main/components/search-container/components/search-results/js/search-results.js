import React from 'react';
import { render } from 'react-dom';

let SearchResults = React.createClass ({
	/*
	* @method getMovieImage
	* @param {String} imageUrl
	* returns a placeholder image or the full path of the image
	*/
	getMovieImage (imageUrl) {
		return imageUrl ? 'http://image.tmdb.org/t/p/w300' + imageUrl : "http://www.citypages.com/img/movie-placeholder.gif"
	},
    render() {
        return <ul className="listed-movies">
        	{this.props.list.map((listItem) => {
	            return  <li key={listItem.id} >
			            	<h3>{listItem.title || listItem.name}</h3>
			            	<h5>{listItem.media_type}</h5>
			            	<img src={this.getMovieImage(listItem.poster_path)}></img>
		            	</li>;
	          })}
        </ul>;
    }
})

export default SearchResults;
