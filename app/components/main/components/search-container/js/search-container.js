import React from 'react';
import { render } from 'react-dom';

import	SearchField from '../components/search-field/js/search-field';
import	SearchResults from '../components/search-results/js/search-results';
import	movieApi from '../../../../themoviedb-api/js/themoviedb-api';


let SearchContainer = React.createClass({
	displayName: 'SearchContainer',
	getInitialState: function() {
		return {
			list: [],
			searchQuery: ''
		};
	},
	/*
	* @method compoenentWillMount
	* requests movies currently playing in cinema
	*/
	componentWillMount(){
		movieApi.getNowPlaying.call(this);
	},
	/*
	* @method searchQuery
	* @param {String} query 
	* searches through database for movies that match the query 
	*/
	searchQueryCall(query) {
		movieApi.searchMovie.call(this, query);

	},
	/*
	* @method handleUserInput
	* @param {String} searchQuery 
	* Makes the actual request for movies and updates the state
	*/
	handleUserInput(searchQuery) {
		this.setState({searchQuery: searchQuery});
		this.searchQueryCall(this.state.searchQuery);
	},
    render() {
        return  <div className="search-container">
		    		<SearchField search={this.state.searchQuery} onUserInput={this.handleUserInput}></SearchField>
		    		<SearchResults list={this.state.list}></SearchResults>
		        </div>;
    }
})

export default SearchContainer;
