import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';

import SearchContainer from '../js/search-container';


describe('Testing search-container component', () => {
   beforeEach(function() {
   		this.searchContainerComponent = TestUtils.renderIntoDocument(
       		<SearchContainer></SearchContainer>
       	);
		this.renderedDOM = () => ReactDOM.findDOMNode(this.searchContainerComponent);
	});
   
   it('should display the search-container component', function() {
       
       expect(this.renderedDOM().tagName).toEqual('DIV');
   });

   it('should have 2 child nodes', function() {
       
       expect(this.renderedDOM().children.length).toEqual(2);
   });

   it('should call getInitialState', function() {
   		console.log("this.getInitialState()", this.renderedDOM.parentNode);
   })
});
