import React from 'react';
import { render } from 'react-dom';

import SearchContainer from '../components/search-container/js/search-container';

class Main extends React.Component {
    render() {
        return <div>
        	<SearchContainer></SearchContainer>
        </div>;
    }
}

export default Main;
