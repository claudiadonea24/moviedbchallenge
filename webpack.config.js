var webpack = require('webpack');
var path = require('path');

var APP_DIR = path.resolve(__dirname,'./app');
var BUILD_DIR = path.resolve(__dirname,'./build');

var config = {
    module: {
        preloaders: [
            {
                test: /\.js$/,
                loaders: ['jshint'],
                exclude: /node_modules/
            }
        ],
        loaders: [
            {
                test: /\.css$/, 
                loader: 'style!css' 
            },
            {
                test: /\.(js|jsx)$/,
                loader: 'babel',
                exclude: /node_modules/
            },
            // {
            //   test: /(\.jsx|\.js)$/,
            //   loader: "eslint-loader",
            //   exclude: /node_modules/
            // },
            {
              test: /\.(jpg|png)$/,
              loader: 'file'
            }

        ]
    },
    entry: [
        'webpack-dev-server/client?http://localhost:8765',
        APP_DIR + ['/js/app.js']
    ],
    devtool: 'source-map',
    output: {
        path: BUILD_DIR,
        filename: 'bundle.js'
    },
    eslint: {
        failOnWarning: false,
        failOnError: true
    },
    plugins:[ new webpack.HotModuleReplacementPlugin()]
};

module.exports = config;